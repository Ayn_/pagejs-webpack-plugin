pagejs-webpack-plugin
===============

Plugin for compiling [PageJS](https://www.npmjs.com/package/@ayn/pagejs) to html files.

## Installation

`npm install pagejs-webpack-plugin`

## Config

Require the package in your webpack file. `const pagejsWebpackPlugin = require('pagejs-webpack-plugin')`.

Include the plugin in your plugin list.

```js
 plugins: [
        new pagejsWebpackPlugin()
    ]
```

You're free to change the default ext if you wish { ext: 'new' } ex: `index.new.js`, 

## Usage

Create file, ex `index.page.js` (`page.js` being the default ext).

Use Page syntax skipping declaring the main object.

```js
({ title: 'My website!' })
    .h1({ style: 'color: blue' }, 'Hello world!')
    .div('How are you today?', (el)=>{
        el.br()
        el.span('Wonderful I hope.')
    })
```

This will output an html file of the same name.

## Template

Feel free to test it out in this demo project:
[PageJS Template Project](https://gitlab.com/Ayn_/pagejs-template)

## License

MIT (http://www.opensource.org/licenses/mit-license.php)