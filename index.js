const { RawSource } = require("webpack-sources")
  , path = require('path')
  , vm = require('vm')
  , pagejs = require('@ayn/pagejs')

  class PagejsWebpackPlugin {
    constructor(config={}) {
      this.config = Object.assign(config, {
        ext: 'page'
      })
    }

    apply(compiler) {
      compiler.hooks.emit.tapAsync('PagejsWebpackPlugin', (compilation, callback) => {
        
        // Explore each chunk (build output):
        compilation.chunks.forEach(chunk => {

          // Explore each chunk (build output):
          compilation.chunks.forEach((chunk) => {

            for(const module of chunk.modulesIterable) {
              // Explore each source file path that was included into the module:
            
              let pagejs_ext = this.config.ext + '.js'

              // Data we need
              let file_name = path.basename(module.rawRequest)
              let content = module._source._value
              let file_ext = file_name.split('.').slice(1).join('.')

              if(file_ext == pagejs_ext) {

                file_name = file_name.slice(0, pagejs_ext.length-2)
            
                // Append pagejs to page
                let context = new vm.createContext({pagejs})
                let script = new vm.Script('new pagejs'+content)

                compilation.assets[file_name+'.html'] = new RawSource(script.runInContext(context).getMarkup())
            
              }
            
            }

          });
          
        });
  
        callback();
      });
    }
  }
  module.exports = PagejsWebpackPlugin;